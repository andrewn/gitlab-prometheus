include_recipe "chef-vault"
include_recipe "gitlab-prometheus::default"

postgres_pw = chef_vault_item("postgres-exporter", "_default")["exporter_user_password"]

directory node["postgres_exporter"]["dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

directory node["postgres_exporter"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

remote_file "#{node['postgres_exporter']['dir']}/postgres_exporter" do
  source "https://github.com/wrouesnel/postgres_exporter/releases/download/v#{node['postgres_exporter']['version']}/postgres_exporter"
  group node["prometheus"]["group"]
  owner node["prometheus"]["user"]
  mode "0755"
  notifies :restart, "runit_service[postgres_exporter]"
end

cookbook_file "#{node['postgres_exporter']['dir']}/queries.yaml" do
  source "postgres_exporter/queries.yaml"
  group node["prometheus"]["group"]
  owner node["prometheus"]["user"]
  mode "0644"
  notifies :reload, "runit_service[postgres_exporter]"
end

include_recipe "runit::default"
runit_service "postgres_exporter" do
  options(
    username: node["postgres_exporter"]["db_user"],
    password: postgres_pw
  )
  default_logger true
  log_dir node["postgres_exporter"]["log_dir"]
end
