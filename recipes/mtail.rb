include_recipe "gitlab-prometheus::default"
include_recipe "runit::default"

directory node["mtail"]["home_dir"] do
  owner node["mtail"]["user"]
  group node["mtail"]["group"]
  mode "0755"
  recursive true
end

directory node["mtail"]["progs_dir"] do
  owner node["mtail"]["user"]
  group node["mtail"]["group"]
  mode "0755"
end

remote_file node["mtail"]["bin"] do
  source node["mtail"]["url"]
  checksum node["mtail"]["sha256sum"]
  owner node["mtail"]["user"]
  group node["mtail"]["group"]
  mode "0755"
  force_unlink true
  notifies :restart, "runit_service[mtail]", :delayed
end

runit_service "mtail" do
  options(
    binary_path: node["mtail"]["bin"]
  )
  sv_timeout 30
  default_logger true
end

logrotate_app "mtail" do
  path "/var/log/mtail/mtail.*"
  options %w(missingok compress delaycompress notifempty)
  rotate 7
  frequency "daily"
end
