# Setup https://github.com/knyar/nginx-lua-prometheus for nginx metrics.
#

# Override chef_nginx to use the distro packages that include LUA.
node.override["nginx"]["repo_source"] = "distro"
node.override["nginx"]["package_name"] = "nginx-extras"
include_recipe "chef_nginx"

lua_dir = File.join(node["nginx"]["dir"], "lua")

directory lua_dir do
  owner "root"
  group "root"
  mode "0755"
  recursive true
end

template "#{node['nginx']['dir']}/sites-available/nginx_lua_exporter" do
  source "nginx_lua_exporter.erb"
  owner  "root"
  group  "root"
  mode   "0644"
  variables lua_dir: lua_dir
  notifies :restart, resources(service: "nginx"), :delayed
end

nginx_site "nginx_lua_exporter" do
  enable true
end

cookbook_file File.join(lua_dir, "prometheus.lua") do
  source "nginx-lua-prometheus/prometheus.lua"
  owner  "root"
  group  "root"
  mode   "0644"
  notifies :restart, resources(service: "nginx"), :delayed
end
