include_recipe "gitlab-prometheus::mtail"

node.default["mtail"]["log_paths"] << "/var/log/nginx/access.log"

cookbook_file "#{node['mtail']['progs_dir']}/nginx.mtail" do
  source "mtail/nginx.mtail"
  notifies :restart, "runit_service[mtail]", :delayed
end
