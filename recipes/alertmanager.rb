require "yaml"
include_recipe "gitlab-vault"
include_recipe "gitlab-prometheus::default"

# Fetch secrets from Chef Vault
alertmanager_conf = GitLab::Vault.get(node, "prometheus-alertmanager", "alertmanager")

peers_search = search(:node, "recipes:gitlab-prometheus\\:\\:alertmanager")
alertmanager_peers = []
if peers_search.empty? || !peers_search.any?
  alertmanager_peers << node["ipaddress"]
else
  peers_search.each do |peer|
    alertmanager_peers << peer["ipaddress"]
  end
end

node.default["alertmanager"]["peers"] = alertmanager_peers.sort.uniq

directory node["alertmanager"]["dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

directory node["alertmanager"]["log_dir"] do
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0755"
  recursive true
end

include_recipe "ark::default"

%w( curl tar bzip2 ).each do |pkg|
  package pkg
end

dir_name = ::File.basename(node["alertmanager"]["dir"])
dir_path = ::File.dirname(node["alertmanager"]["dir"])

ark dir_name do
  url node["alertmanager"]["binary_url"]
  checksum node["alertmanager"]["checksum"]
  version node["alertmanager"]["version"]
  prefix_root Chef::Config["file_cache_path"]
  path dir_path
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  #  extension node['prometheus']['file_extension'] unless node['prometheus']['file_extension'].empty?
  action :put
end

template node["alertmanager"]["flags"]["config.file"] do
  source "alertmanager.yml.erb"
  owner node["prometheus"]["user"]
  group node["prometheus"]["group"]
  mode "0644"
  variables(conf: alertmanager_conf)
end

# mash name is nil if set as an ohai attribute
node.default["alertmanager"]["flags"]["mesh.nickname"] = node.name
include_recipe "runit::default"
runit_service "alertmanager" do
  default_logger true
  log_dir node["alertmanager"]["log_dir"]
end
