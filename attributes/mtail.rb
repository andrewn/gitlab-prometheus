# mtail directories
default["mtail"]["home_dir"] = "/opt/prometheus/mtail"
default["mtail"]["progs_dir"] = "#{node['mtail']['home_dir']}/progs"
default["mtail"]["log_dir"] = "/var/log/mtail"
default["mtail"]["log_paths"] = [] # overridden by specialized recipes

# mtail binary
default["mtail"]["bin"] = "#{node['mtail']['home_dir']}/mtail"

# mtail artifact
default["mtail"]["url"] = "https://gitlab.com/gl-infra/mtail/-/jobs/26202910/artifacts/raw/build/mtail"
default["mtail"]["sha256sum"] = "2cd8dd1aafdddcb4f1b8770cfeddf6abd02b3f2407dee5c647b3f4cf8320c247"

# mtail user / group
default["mtail"]["user"] = "root"
default["mtail"]["group"] = "root"
