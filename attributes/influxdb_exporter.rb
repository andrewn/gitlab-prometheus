default["influxdb_exporter"]["checksum"]    = "3c3a898905964063e732910e4ebffc02d5af9a910892d63227eeefb6df255988"
default["influxdb_exporter"]["dir"]         = "/opt/prometheus/influxdb_exporter"
default["influxdb_exporter"]["binary"]      = "#{node['influxdb_exporter']['dir']}/influxdb_exporter"
default["influxdb_exporter"]["log_dir"]     = "/var/log/prometheus/influxdb_exporter"
default["influxdb_exporter"]["version"]     = "0.1.0"
default["influxdb_exporter"]["binary_url"]  = "https://github.com/prometheus/influxdb_exporter/releases/download/v#{node['influxdb_exporter']['version']}/influxdb_exporter-#{node['influxdb_exporter']['version']}.linux-amd64.tar.gz"

# You can add any flags to the runit script by adding another flag attribute
# default["influxdb_exporter"]["flags"]["your.config.option"] = "value"
# Please see the docs at https://github.com/prometheus/influxdb_exporter

default["influxdb_exporter"]["flags"]["web.listen-address"] = "0.0.0.0:9122"
