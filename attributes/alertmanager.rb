#
# Cookbook Name GitLab::Monitoring
# Attributes:: alertmanager
#

default["alertmanager"]["dir"]         = "/opt/prometheus/alertmanager"
default["alertmanager"]["binary"]      = "#{node['alertmanager']['dir']}/alertmanager"
default["alertmanager"]["log_dir"]     = "/var/log/prometheus/alertmanager"

default["alertmanager"]["version"]     = "0.8.0"
default["alertmanager"]["checksum"]    = "b569ae0d31e5df59391124578bcf9b28316cf12adb4cc9d10f439fc3ab3422f1"
default["alertmanager"]["binary_url"]  = "https://github.com/prometheus/alertmanager/releases/download/v#{node['alertmanager']['version']}/alertmanager-#{node['alertmanager']['version']}.linux-amd64.tar.gz"

default["alertmanager"]["flags"]["config.file"]        = "#{node['alertmanager']['dir']}/alertmanager.yml"
default["alertmanager"]["flags"]["web.listen-address"] = "0.0.0.0:9093"
default["alertmanager"]["flags"]["storage.path"] = "#{node['alertmanager']['dir']}/data"

default["alertmanager"]["peers"] = []

default["alertmanager"]["slack"]["api_url"] = nil
