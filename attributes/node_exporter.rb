default["node_exporter"]["checksum"]    = "d5980bf5d0dc7214741b65d3771f08e6f8311c86531ae21c6ffec1d643549b2e"
default["node_exporter"]["dir"]         = "/opt/prometheus/node_exporter"
default["node_exporter"]["binary"]      = "#{node['node_exporter']['dir']}/node_exporter"
default["node_exporter"]["log_dir"]     = "/var/log/prometheus/node_exporter"
default["node_exporter"]["version"]     = "0.14.0"
default["node_exporter"]["binary_url"]  = "https://github.com/prometheus/node_exporter/releases/download/v#{node['node_exporter']['version']}/node_exporter-#{node['node_exporter']['version']}.linux-amd64.tar.gz"

# You can add any flags to the runit script by adding another flag attribute
# default["node_exporter"]["flags"]["your.config.option"] = "value"
# Please see the docs at https://github.com/prometheus/node_exporter#collectors

default["node_exporter"]["flags"]["web.listen-address"] = "0.0.0.0:9100"
default["node_exporter"]["flags"]["collector.textfile.directory"] = "#{node['node_exporter']['dir']}/metrics"
default["node_exporter"]["flags"]["collectors.enabled"] = "conntrack,diskstats,edac,entropy,filefd,filesystem,hwmon,infiniband,loadavg,mdadm,meminfo,netdev,netstat,sockstat,stat,textfile,time,uname,vmstat,wifi,zfs,nfs"
default["node_exporter"]["flags"]["collector.filesystem.ignored-fs-types"] = "\"^(nfs.|fuse.lxcfs|rpc_pipefs|(sys|proc|auto)fs)$\""

default["node_exporter"]["ntpd_metrics_logdir"] = "/var/log/prometheus/node_exporter_ntpd_metrics"
