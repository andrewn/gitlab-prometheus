require "spec_helper"
require "chef-vault"

describe "gitlab-prometheus::prometheus-haproxy" do
  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
        node.set["haproxy"]["incoming_port"] = 12345
        node.set["haproxy"]["servers"] = "myrole"
      }.converge(described_recipe)
    end

    it "creates the haproxy.conf in the default location" do
      expect(chef_run).to create_template("/etc/haproxy/haproxy.cfg").with(
        mode: "0600"
      )
    end
  end
end
