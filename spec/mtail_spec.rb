require "spec_helper"

describe "gitlab-prometheus::mtail" do
  context "default execution" do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new { |node|
      }.converge(described_recipe)
    end

    it "creates mtail home dir" do
      expect(chef_run).to create_directory("/opt/prometheus/mtail")
    end

    it "creates the mtail dir in the configured location" do
      expect(chef_run).to create_directory("/opt/prometheus/mtail/progs")
    end

    it "creates the runit service" do
      expect(chef_run).to enable_runit_service("mtail")
    end
  end
end
