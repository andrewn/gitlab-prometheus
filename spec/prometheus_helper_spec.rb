require "spec_helper"
require "prometheus_helper"

describe "prometheus_helper" do
  before do
    allow_any_instance_of(Chef::Search::Query)
      .to receive(:search).with(:node, "roles:myrole")
                          .and_return([{ "fqdn" => "my.beautiful.server",
                                         "hostname" => "my.hostname",
    }])
    allow_any_instance_of(Chef::Search::Query)
      .to receive(:search).with(:node, "roles:empty-role")
                          .and_return([])
    allow_any_instance_of(Chef::Search::Query)
      .to receive(:search).with(:node, "attribute:true")
                          .and_return([{ "fqdn" => "my.beautiful.server.attribute",
                                         "hostname" => "my.hostname",
    }])
  end

  let(:prometheus_module) do
    c = Class.new { include Gitlab::Prometheus }
    c.new
  end

  it "can load at least one node" do
    search_query = [{ "fqdn" => "my.beautiful.server", "hostname" => "my.hostname" }]
    exporter_port = 9100
    public_hosts = []
    inventory = prometheus_module.generate_inventory_file(search_query, exporter_port, public_hosts)
    expect(inventory).to eq([{ "targets" => ["192.168.1.2:9100"],
                               "labels" => { "instance" => "my.beautiful.server:9100",
                                             "fqdn" => "my.beautiful.server" } },
    ])
  end

  it "can load one node with a public ip" do
    search_query = [{ "fqdn" => "my.beautiful.server", "hostname" => "my.hostname" }]
    exporter_port = 9100
    public_hosts = ["my.beautiful.server"]
    inventory = prometheus_module.generate_inventory_file(search_query, exporter_port, public_hosts)
    expect(inventory).to eq([{ "targets" => ["my.beautiful.server:9100"],
                               "labels" => { "fqdn" => "my.beautiful.server" } },
    ])
  end

  it "fails to load a node without an ip" do
    search_query = []
    exporter_port = 9100
    public_hosts = []
    inventory = prometheus_module.generate_inventory_file(search_query, exporter_port, public_hosts)
    expect(inventory).to be_empty
  end

  it "searchs for an attribute" do
    search_query = [{ "fqdn" => "my.beautiful.server.attribute",
                      "hostname" => "my.hostname" }]
    exporter_port = 80
    public_hosts = []
    inventory = prometheus_module.generate_inventory_file(search_query, exporter_port, public_hosts)
    expect(inventory).to eq([{ "targets" => ["192.168.1.2:80"],
                               "labels" => { "instance" => "my.beautiful.server.attribute:80",
                                             "fqdn" => "my.beautiful.server.attribute" } },
    ])
  end
end
